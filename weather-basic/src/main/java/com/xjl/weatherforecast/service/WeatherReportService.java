package com.xjl.weatherforecast.service;

import com.xjl.weatherforecast.vo.Weather;

public interface WeatherReportService {


    Weather getDataByCityId(String cityId);
}
